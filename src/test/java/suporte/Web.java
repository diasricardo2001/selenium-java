package suporte;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Web {
    public static WebDriver createChrome() {
        //Abrindo o navegador
        System.setProperty("webdriver.chrome.driver", "C:\\projetos\\automacao\\curso-selenium\\drivers\\chromedriver.exe");
        WebDriver navegador = new ChromeDriver(); // Instanciando um navegador, quando executada abre uma pagina no Chrome
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Navegando para a página do Taskit!
        navegador.get("http://www.juliodelima.com.br/taskit");
        navegador.manage().window().maximize();

        return navegador;
    }
}
