package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginFormPage extends BasePage{

    public LoginFormPage(WebDriver navegador) {
        super(navegador);
    }

    public LoginFormPage digitarLogin(String login) {
        navegador.findElement(By.id("signinbox")).findElement(By.name("login")).sendKeys(login);
        return this;
    }

    public LoginFormPage digitarSenha (String password) {
        navegador.findElement(By.id("signinbox")).findElement(By.name("password")).sendKeys(password);
        return this;
    }

    public SecretPage clickLogar() {
        navegador.findElement(By.id("signinbox")).findElement(By.linkText("SIGN IN")).click();
        return new SecretPage(navegador);
    }

    /**
     * Metodo Funcional para Realizar login, ultilizando os metodos estruturais
     * @param login
     * @param senha
     * @return
     */
    public SecretPage realizarLogin(String login, String senha) {
        digitarLogin(login);
        digitarSenha(senha);
        clickLogar();

        return new SecretPage(navegador);
    }
}
