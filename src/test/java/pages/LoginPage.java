package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

    /**
     * Toda classe dentro do page object ira ter no minimo essa estrutura,
     * Um atributo do tipo WebDriver e um construtor com esse atributo
     * Cada metodo dentro do page object deve retornar a page que ira navegar, mesmo se for ele mesmo
     *
     */
public class LoginPage extends BasePage{

        public LoginPage(WebDriver navegador) {
            super(navegador);
        }

        public LoginFormPage clicarSignIn() {
        navegador.findElement(By.linkText("Sign in")).click();
        return new LoginFormPage(navegador);
    }
}
