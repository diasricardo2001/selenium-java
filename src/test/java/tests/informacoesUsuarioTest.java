package tests;

/*
  Para não precisar importar um um oque precisa do Assert import de forma estatica
  import static org.junit.Assert.*;
 */

import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.*;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import suporte.Generator;
import suporte.Screenshot;
import suporte.Web;

import java.util.concurrent.TimeUnit;
@RunWith(DataDrivenTestRunner.class) // Apontar a classe de teste do easyTest
@DataLoader(filePaths = "InformacoesUsuarioTest.csv") // Apontando os arquivos que vão ser repositorio para os dados do teste

public class informacoesUsuarioTest {
    private WebDriver navegador;

    /**
     * Toda classe de teste termina em seu nome Test
     * Todo metodo de teste tem que ser publico
     * Metodos de testes não podem retornar nenhum valor
     * Todo metodo de teste deve começar com o prefixo "test"
     * Todo metodo de teste deve conter uma validação explicita nele dizendo se passa ou falha
     * Ultilizar a classe Assert, nela contem metodos prontos para ultilização dos testes
     */

    @Rule
    public TestName test = new TestName();

    @Before
    public void setUp() {
        navegador = Web.createChrome();

        // Clicar no link que possui o texto "Sing in"
        // Vareavel do tipo WebElement recebe se foi localizado e depois clica
        navegador.findElement(By.linkText("Sign in")).click();

        // Indentificar o formulario de Login
        WebElement formularioSignInBox = navegador.findElement(By.id("signinbox"));

        // Digitar no campo com name "login" que está dentro do formúlario de id "signinbox" o texto "userTest01"
        formularioSignInBox.findElement(By.name("login")).sendKeys("userTest01");

        // Digitar no campo com name "password" que está dentro do formúlario de id "signinbox" o texto "teste1234"
        formularioSignInBox.findElement(By.name("password")).sendKeys("teste1234");

        // Clicar no link com o texto "SIGN IN"
        navegador.findElement(By.linkText("SIGN IN")).click();

        // Validar que dentro do elemento com class "me" está o texto "Hi, Usuario Teste"
        // WebElement me = navegador.findElement(By.className("me"));
        // String textoNoElementoMe = me.getText();
        // Assert.assertEquals("Hi, Usuario Teste", textoNoElementoMe);

        // Clicar em um link que possui a class "me"
        navegador.findElement(By.className("me")).click();

        // Clicar em um link que possui o texto "MORE DATE ABOUT YOU"
        navegador.findElement(By.linkText("MORE DATA ABOUT YOU")).click();
    }

    @Test
    public void testAdicionarUmaInformacaoAdicionalDoUsuario(@Param(name="tipo") String tipo, @Param(name="contato") String contato, @Param(name="mensagem") String mensagemExperada) {

        // Clicar em um button através do seu Xpath (//button[@data-target="addmoredata"])
        navegador.findElement(By.xpath("//button[@data-target=\"addmoredata\"]")).click();

        // Indentificar a Popup onde esta o formulário de id "addmoredata"
        WebElement popupAddMoreDate = navegador.findElement(By.id("addmoredata"));

        // Na combo de name "type" escolher a opção "Phone"
        WebElement campoType = popupAddMoreDate.findElement(By.name("type"));
        new Select(campoType).selectByVisibleText(tipo);

        // Indentificar e inserir no campo de name "contact" digitar "+5511999999999"
        popupAddMoreDate.findElement(By.name("contact")).sendKeys(contato);

        // Clicar no link de text "SAVE" que esta na popup
        popupAddMoreDate.findElement(By.linkText("SAVE")).click();

        // Na mensagem de id "toast-container" validar que o texto  é "Your contact has been added!"
        WebElement mensagemPop = navegador.findElement(By.id("toast-container"));
        String mensagem = mensagemPop.getText();
        Assert.assertEquals(mensagemExperada, mensagem);
    }

    @Test
    public void removerUmaInformacaoDeUmUsuario() {

        // preceding, consigo pegar o elemento antes do pai
        // following, consigo pegar o elemento depois do pai
        // Remover o Numero +5511977774444
        // Xpth: //span[text()="+5511977774444"]following-sibling::a
        // neste Xpth, eu passo a tag pai, com o metodo text() onde passando o valor dentro de aspas ele realiza a
        // busca na DOM, em seguinda usando o following-sibling consigo passar assim qual a tag depois do pai que eu
        // desejo ultilizar

        // Clicar no elemento pelo seu Xpath
        navegador.findElement(By.xpath("//span[text()='+5511977774444']/following-sibling::a")).click();

        // Confirmar a janela javaScripit
        navegador.switchTo().alert().accept();

        // Validar mensagem apresentada foi "Rest in peace, dear phone!"
        WebElement mensagemPop = navegador.findElement(By.id("toast-container"));
        String mensagem = mensagemPop.getText();
        Assert.assertEquals("Rest in peace, dear phone!", mensagem);

        // Tirando print para Evidencia
        String screenshotArquivo = "C:\\Users\\Ricardo\\Documents\\automacao\\curso-selenium\\test-report\\taskit\\" + Generator.dataHoraParaArquivo() + test.getMethodName() + ".png";
        Screenshot.take(navegador, screenshotArquivo);

        // Aguardar até 10 segundos para que a janela desapareça
        WebDriverWait aguardar = new WebDriverWait(navegador, 10);
        aguardar.until(ExpectedConditions.stalenessOf(mensagemPop));

        // Clicar no link com o texto Logout
        navegador.findElement(By.linkText("Logout")).click();
    }

    @After
    public void tearDown() {
        //Fechar navegador
        navegador.quit();
    }
}
